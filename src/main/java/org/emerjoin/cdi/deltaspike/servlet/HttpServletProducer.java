package org.emerjoin.cdi.deltaspike.servlet;

import javax.enterprise.context.ContextNotActiveException;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Produces;
import javax.servlet.http.HttpServletRequest;

public class HttpServletProducer {

    @Produces @RequestScoped
    public HttpServletRequest produce(){
        HttpServletRequest request = DeltaspikeCdiRequestContextFilter.HTTP_SERVLET_REQUEST;
        if(request==null)
            throw new ContextNotActiveException("RequestScoped context is not active");
        return request;
    }

}
