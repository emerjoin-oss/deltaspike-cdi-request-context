package org.emerjoin.cdi.deltaspike.servlet;

import org.apache.deltaspike.cdise.api.CdiContainer;
import org.apache.deltaspike.cdise.api.CdiContainerLoader;

import javax.enterprise.context.RequestScoped;
import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@WebFilter(urlPatterns = "/*")
public class DeltaspikeCdiRequestContextFilter implements Filter {

    public static HttpServletRequest HTTP_SERVLET_REQUEST;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        CdiContainer cdiContainer = CdiContainerLoader.getCdiContainer();
        try {
            HTTP_SERVLET_REQUEST = (HttpServletRequest) servletRequest;
            cdiContainer.getContextControl().startContext(RequestScoped.class);
            filterChain.doFilter(servletRequest, servletResponse);
            HTTP_SERVLET_REQUEST = null;
        }finally {
            cdiContainer.getContextControl().stopContext(RequestScoped.class);
        }
    }

    @Override
    public void destroy() {

    }
}
