# Why
When you are running Deltaspike CDI Unit tests of Servlet based applications or components that
integrate with Servlet-api injectables, you need to do two things:
* Initiate and terminate the RequestScoped context on every Request
* Create an HttpServletRequest producer, but only if you are injecting into your components

This library helps you do exactly that. 

# Components
This library contains essentially two components:
* DeltaspikeCdiRequestContextFilter - controls the RequestScoped context. It activates the context when a Request comes in and then it deactivates the context once the request ends.
* HttpServletProducer - produces an HttpServletRequest


# How to use
Just include it in your project, register and register the **DeltaspikeCdiRequestContextFilter** and you are good to go.